from __future__ import print_function
import sys

print("'masai.core.tools' is deprecated, use 'masai.core.utils' instead.", file=sys.stderr)
