!===============================================================================
! pulsar.forpulsar
!===============================================================================
! Copyright (C) 2006-2013 Christian Fernandez, Jean-Paul Amoureux
! JPA - Unite de Catalyse et Chimie du Solide, Lille, France.
! CF  - Laboratoire Catalyse et Spectrochimie, Caen, France.  
!       christian.fernandez@ensicaen.fr
! This software is governed by the CeCILL-B license under French law 
! and abiding by the rules of distribution of free software.  
! You can  use, modify and/ or redistribute the software under 
! the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA
! at the following URL "http://www.cecill.info".
! See Licence.txt in the main masai source directory
!===============================================================================

include "diagonalize.f90" 
include "util.f90"
include "parameters.f90"
include "share.f90"
include "operators.f90"
include "ll.f90"
include "main.f90"

!===============================================================================
program Main
  ! This main program will not be used 
  ! as a librairy (so, pyd, dll ...) will be created with f2y
end program
!===============================================================================

	
