# -*- coding: utf-8 -*-

"""A one-line description.

A longer description that spans multiple lines.  Explain the purpose of the
file and provide a short list of the key classes/functions it contains.  This
is the docstring shown when some does 'import foo;foo?' in IPython, so it
should be reasonably useful and informative.
"""
# Copyright (c) C. Fernandez' group @ LCS.
# Distributed under the terms of the CeCILL-B License.

from __future__ import print_function

# -----------------------------------------------------------------------------
# Globals
# -----------------------------------------------------------------------------

# stdlib

# third-party

# -----------------------------------------------------------------------------
# Local utilities and constants
# -----------------------------------------------------------------------------

# constants
DEBUG = True
__author__ = 'christian'
__all__ = []
