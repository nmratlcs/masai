##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr
$$ /home/nmr/data/siblanixenon/nmr/Y1a-QIN/164/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-10-21 13:15:18.632 +0200>,<nmr>,<lcs175n>,<go>,<TOPSPIN 3.2>,
      <created by zg
	started at 2014-10-21 13:14:10.434 +0200,
	POWCHK enabled, PULCHK disabled,
       configuration hash MD5:
       6F 65 75 CC 89 0E 6F 2A E6 D1 28 7C 9E 2C 1B 25
       data hash MD5: 12K
       9A E4 C2 A3 A3 FA 31 24 F4 75 76 B9 42 05 90 17>)
(   2,<2014-10-21 13:15:49.693 +0200>,<nmr>,<lcs175n>,<proc1d>,<TOPSPIN 3.2>,
      <Start of raw data processing
       efp BC_mod = 2 LB = 0 FT_mod = 6 PKNL = 1 PHC0 = 8.509561 PHC1 = 72.20047 SI = 8K 
       data hash MD5: 8K
       82 4E DA E1 BD B3 70 AB BE C5 87 96 72 7F CC 75>)
##END=

$$ hash MD5
$$ E5 83 C0 E9 41 63 61 32 09 D1 51 E6 07 80 0E 1C
