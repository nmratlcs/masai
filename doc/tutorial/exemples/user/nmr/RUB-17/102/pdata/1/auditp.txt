##TITLE= Audit trail, TOPSPIN		Version 3.2
##JCAMPDX= 5.01
##ORIGIN= Bruker BioSpin GmbH
##OWNER= nmr
$$ /home/nmr/data/anaP/nmr/VPI-7/102/pdata/1/auditp.txt
##AUDIT TRAIL=  $$ (NUMBER, WHEN, WHO, WHERE, PROCESS, VERSION, WHAT)
(   1,<2014-06-02 13:56:51.163 +0200>,<nmr>,<lcs185n>,<go>,<TOPSPIN 3.2>,
      <created by zg
	started at 2014-05-27 17:43:11.344 +0200,
	POWCHK enabled, PULCHK disabled,
       terminated by command 'stop'
       configuration hash MD5:
       4B 25 6E 9E 22 16 69 0B 1E C8 AC 62 38 01 26 53
       data hash MD5: 4K * 59
       97 A7 71 F8 AE 4A 20 30 D3 32 B3 0C 5A 34 F5 02>)
(   2,<2015-01-15 17:52:35.502 +0100>,<nmr>,<lcs185n>,<proc2d>,<TOPSPIN 3.2>,
      <Start of raw data processing
       xfb F2: SI = 8K WDW = 1 LB = 5 FT_mod = 6 PKNL = 1 PHC0 = 40.99791 PHC1 = 38.39999 F1: SI = 1K WDW = 1 LB = 5 FT_mod = 6 FCOR = 0.5 PHC0 = 18.87071 PHC1 = -48.8
       data hash MD5: 8K * 1K
       58 A4 1D C0 9D BF 98 75 C0 71 A9 67 79 CE 02 44>)
##END=

$$ hash MD5
$$ AF D1 5C E1 F1 3E 40 47 4A 0D A3 B1 D7 00 07 1C
